package br.com.gympersonal.app.core;

import br.com.gympersonal.app.model.entity.Aluno;
import br.com.gympersonal.app.model.entity.Personal;
import br.com.gympersonal.app.model.entity.Pessoa;

import br.com.gympersonal.app.rest.dto.*;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        modelMapper
                .createTypeMap(Pessoa.class, AdminDTO.class)
                .<String>addMapping(src -> src.getUsuario().getUsername(),
                        (dest, value) -> dest.setEmail(value))
                .<String>addMapping(src -> src.getUsuario().getIsAdmin(),
                        (dest, value) -> dest.setIsAdmin(value));

        modelMapper
                .createTypeMap(Personal.class, PersonalModel.class)
                .<String>addMapping(src -> src.getUsuario().getUsername(),
                        (dest, value) -> dest.setEmail(value))
                .<String>addMapping(src -> src.getUsuario().getIsAdmin(),
                        (dest, value) -> dest.setIsAdmin(value))
                .<String>addMapping(src -> src.getUsuario().getPessoa().getSexo(),
                        (dest, value) -> dest.setSexo(value));
        modelMapper
                .createTypeMap(Personal.class, PersonalDTO.class);
        
        modelMapper
		        .createTypeMap(Aluno.class, AlunoModel.class)
		        .<String>addMapping(src -> src.getUsuario().getUsername(),
		                (dest, value) -> dest.setEmail(value))
		        .<String>addMapping(src -> src.getUsuario().getIsAdmin(),
		                (dest, value) -> dest.setIsAdmin(value))
		        .<String>addMapping(src -> src.getUsuario().getPessoa().getSexo(),
		                (dest, value) -> dest.setSexo(value));
        modelMapper
                .createTypeMap(Aluno.class, AlunoDTO.class);

        return modelMapper;
    }

}
