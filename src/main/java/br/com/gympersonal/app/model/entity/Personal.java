package br.com.gympersonal.app.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@PrimaryKeyJoinColumn(name="idPessoa")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Personal extends Pessoa {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="plano_id")
    @JsonManagedReference
    private Plano plano;
    
    @OneToMany(mappedBy="personal", fetch = FetchType.LAZY)
    private java.util.List<Aluno> alunos;

}