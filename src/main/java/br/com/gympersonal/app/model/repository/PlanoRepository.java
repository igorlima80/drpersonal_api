package br.com.gympersonal.app.model.repository;

import br.com.gympersonal.app.model.entity.Plano;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PlanoRepository extends JpaRepository<Plano, Integer> {
    @Query("SELECT count(p) FROM Personal p WHERE p.plano.id = :id")
    Integer buscarQuantidadePersonais(int id);
}
