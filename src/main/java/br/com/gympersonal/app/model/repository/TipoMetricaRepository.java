package br.com.gympersonal.app.model.repository;

import br.com.gympersonal.app.model.entity.TipoMetrica;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipoMetricaRepository extends JpaRepository<TipoMetrica, Integer> {
    boolean existsById(Integer id);
}
