package br.com.gympersonal.app.model.repository;

import br.com.gympersonal.app.model.entity.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlunoRepository extends JpaRepository<Aluno, Integer> {
    Integer countByPersonalId(int id);
}
