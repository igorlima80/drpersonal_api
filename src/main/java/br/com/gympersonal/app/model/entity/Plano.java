package br.com.gympersonal.app.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
public class Plano {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private Integer limiteClientes;

    @Column(nullable = false)
    private BigDecimal valor;

    @Column(nullable = false)
    private String nome;

    @Enumerated(EnumType.STRING)
    private StatusPlano status;

    @ManyToOne
    @JoinColumn(name = "id_ciclo_vencimento")
    private CicloVencimento cicloVencimento;




}
