package br.com.gympersonal.app.model.entity;

public enum StatusUsuario {
        ATUALIZAR_SENHA, INATIVO, ATIVO, PAGAMENTO_PENDENTE;
}
