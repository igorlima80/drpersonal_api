package br.com.gympersonal.app.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Foto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 150)
    private String nome;

    @Column(nullable = true, length = 150)
    private String url;

    @ManyToOne
    @JoinColumn(name="exercicio_id")
    private Exercicio exercicio;

    public Foto(String nome, String url) {
        this.nome = nome;
        this.url = url;
    }


}
