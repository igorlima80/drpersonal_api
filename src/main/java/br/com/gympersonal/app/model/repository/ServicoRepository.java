package br.com.gympersonal.app.model.repository;

import br.com.gympersonal.app.model.entity.Servico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServicoRepository extends JpaRepository<Servico, Integer> {
}
