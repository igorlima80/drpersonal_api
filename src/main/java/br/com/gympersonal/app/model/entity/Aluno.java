package br.com.gympersonal.app.model.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name="idPessoa")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Aluno extends Pessoa {
	
	@ManyToOne
    @JoinColumn(name="personal_id")
    private Personal personal;

}

