package br.com.gympersonal.app.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Objetivo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 150)
    private String nome;
}
