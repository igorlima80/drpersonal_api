package br.com.gympersonal.app.model.repository;


import br.com.gympersonal.app.model.entity.Exercicio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExercicioRepository extends JpaRepository<Exercicio, Integer> {
    List<Exercicio> findByPersonalIsNull();

    List<Exercicio> findByPersonalId(int id);

    List<Exercicio> findByGrupos_id(int id);

    List<Exercicio> findByPersonalIsNullAndGrupos_id(int id);

    List<Exercicio> findByPersonalIdAndGrupos_id(int idPersonal, int idGrupo);

}