package br.com.gympersonal.app.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CicloVencimento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 150)
    @NotBlank(message = "{campo.descricao.obrigatorio}")
    private String descricao;

    @Column(nullable = false, length = 150)
    private Integer quantidadeDias;

}
