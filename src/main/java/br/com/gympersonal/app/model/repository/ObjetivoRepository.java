package br.com.gympersonal.app.model.repository;

import br.com.gympersonal.app.model.entity.Objetivo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ObjetivoRepository extends JpaRepository<Objetivo, Integer> {

}
