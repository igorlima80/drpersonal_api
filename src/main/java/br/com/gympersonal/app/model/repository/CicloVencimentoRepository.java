package br.com.gympersonal.app.model.repository;

import br.com.gympersonal.app.model.entity.CicloVencimento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CicloVencimentoRepository extends JpaRepository<CicloVencimento, Integer> {
}
