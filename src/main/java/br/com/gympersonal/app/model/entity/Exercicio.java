package br.com.gympersonal.app.model.entity;

import lombok.Data;
import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Exercicio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 150)
    private String nome;

    @Enumerated(EnumType.STRING)
    private StatusExercicio status;

    @ManyToOne
    @JoinColumn(name = "id_tipo_exercicio")
    private TipoExercicio tipoExercicio;

    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name = "id_tipo_metrica")
    private TipoMetrica tipoMetrica;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "exercicio_grupo",
            joinColumns = @JoinColumn(name = "exercicio_id"),
            inverseJoinColumns = @JoinColumn(name = "grupo_id"))
    private List<GrupoMuscular> grupos;

    @Column(nullable = true)
    private String caminhoVideo;

    @OneToMany(mappedBy="exercicio", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Foto> fotos;

    @ManyToOne
    @JoinColumn(name = "id_personal")
    private Personal personal;

}
