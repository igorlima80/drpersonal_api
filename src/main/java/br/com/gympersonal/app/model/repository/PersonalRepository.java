package br.com.gympersonal.app.model.repository;

import br.com.gympersonal.app.model.entity.Personal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonalRepository extends JpaRepository<Personal, Integer> {
    Integer countByPlanoId(int id);

    List<Personal> findByUsuarioUsernameLike(String email);

    List<Personal> findByNomeLike(String nome);

    List<Personal> findByCpfLike(String cpf);
}
