package br.com.gympersonal.app.model.entity;

public enum Sexo {
    MASCULINO, FEMININO;
}
