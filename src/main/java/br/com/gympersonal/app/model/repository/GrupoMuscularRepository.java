package br.com.gympersonal.app.model.repository;

import br.com.gympersonal.app.model.entity.GrupoMuscular;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GrupoMuscularRepository extends JpaRepository<GrupoMuscular, Integer> {

}
