package br.com.gympersonal.app.model.entity;

public enum StatusPlano {
    INATIVO, ATIVO;
}
