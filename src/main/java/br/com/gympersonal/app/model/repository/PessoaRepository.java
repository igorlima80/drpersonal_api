package br.com.gympersonal.app.model.repository;


import br.com.gympersonal.app.model.entity.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {

    @Query("SELECT p FROM Pessoa p WHERE p.usuario.isAdmin = true")
    List<Pessoa> buscarAdministradores();

}
