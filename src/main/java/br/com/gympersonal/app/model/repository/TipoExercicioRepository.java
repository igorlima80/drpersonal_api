package br.com.gympersonal.app.model.repository;

import br.com.gympersonal.app.model.entity.TipoExercicio;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipoExercicioRepository extends JpaRepository<TipoExercicio, Integer> {
    boolean existsById(Integer id);
}
