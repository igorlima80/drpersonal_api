package br.com.gympersonal.app.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()

                .antMatchers("/api/usuarios/**").permitAll()
                .antMatchers("/api/admins/**").permitAll()
                .antMatchers("/api/ciclos-vencimento/**").permitAll()
                .antMatchers("/api/planos/**").permitAll()
                .antMatchers("/api/personais/**").permitAll()
                .antMatchers("/api/alunos/**").permitAll()
                .antMatchers("/oauth/**").permitAll()
                .antMatchers("/**").permitAll()
                .anyRequest().permitAll()
                .and().cors()
                .and().csrf().disable();
    }



}
