package br.com.gympersonal.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class GymPersonalApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {

        SpringApplication.run(GymPersonalApplication.class, args);
    }



}
