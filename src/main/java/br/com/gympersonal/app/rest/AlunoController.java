package br.com.gympersonal.app.rest;


import java.util.List;
import java.util.stream.Collectors;
import br.com.gympersonal.app.model.repository.UsuarioRepository;
import br.com.gympersonal.app.service.AlunoService;
import br.com.gympersonal.app.service.PersonalService;
import br.com.gympersonal.app.model.entity.Aluno;
import br.com.gympersonal.app.model.repository.AlunoRepository;
import br.com.gympersonal.app.rest.dto.AlunoDTO;
import br.com.gympersonal.app.rest.dto.AlunoModel;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import br.com.gympersonal.app.model.entity.StatusUsuario;
import lombok.RequiredArgsConstructor;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/alunos")
@RequiredArgsConstructor
public class AlunoController {
	
	 @Autowired
	 private AlunoRepository repository;

	 @Autowired
	 private UsuarioRepository usuarioRepository;

	 @Autowired
	 private AlunoService service;

	 @Autowired
	 private PersonalService personalService;


	 @Autowired
	 private ModelMapper modelMapper;
	 
	 @PostMapping
	 @ResponseStatus(HttpStatus.CREATED)
	 public AlunoModel salvar(@Valid @RequestBody AlunoDTO dto){
		 if(usuarioRepository.existsByUsername(dto.getEmail()) == true){
			 throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O E-mail informado já existe no banco de dados");
		 }

		 if(!personalService.existsById(dto.getPersonalId())){
			 throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O Personal com ID:" + dto.getPersonalId() +  " não existe no banco de dados");
		 }

		  return service.save(dto);
	 }

	 @GetMapping("{id}")
	 public AlunoModel buscarPorId(@PathVariable Integer id){
		 return service.find(id);
	 }
	    
	 @DeleteMapping("{id}")
	 @ResponseStatus(HttpStatus.NO_CONTENT)
	 public void deletar(@PathVariable Integer id){
		 service.delete(id);
	  }
	 
	    @PutMapping("{id}")
	    @ResponseStatus(HttpStatus.NO_CONTENT)
	    public void atualizar(@PathVariable Integer id, @RequestBody AlunoDTO alunoAtualizado){
	        service.update(id, alunoAtualizado);
	    }
	    
	    @GetMapping
	    public List<AlunoModel> listarTodos(){
	        return repository
	                .findAll()
	                .stream()
	                .map(this::toAlunoModel)
	                .collect(Collectors.toList());
	    }

	@PostMapping(path="/{id}/ativar")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void ativar(@PathVariable Integer id){
		repository
				.findById(id)
				.map(aluno -> {
					aluno.getUsuario().setStatus(StatusUsuario.ATIVO);

					return usuarioRepository.save(aluno.getUsuario());
				})
				.orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	@PostMapping(path="/{id}/desativar")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void desativar(@PathVariable Integer id){
		repository
				.findById(id)
				.map(aluno -> {
					aluno.getUsuario().setStatus(StatusUsuario.INATIVO);

					return usuarioRepository.save(aluno.getUsuario());
				})
				.orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}
	    
	
	  private AlunoDTO toAlunoDTO(Aluno aluno){
	        return modelMapper.map(aluno, AlunoDTO.class);
	    }

	    private AlunoModel toAlunoModel(Aluno aluno){
	        return modelMapper.map(aluno, AlunoModel.class);
	    }

	    private Aluno toAluno(AlunoDTO aluno){
	        return modelMapper.map(aluno, Aluno.class);
	    }



}
