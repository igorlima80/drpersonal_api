package br.com.gympersonal.app.rest;


import br.com.gympersonal.app.model.repository.PersonalRepository;
import br.com.gympersonal.app.model.repository.PlanoRepository;
import br.com.gympersonal.app.model.repository.UsuarioRepository;
import br.com.gympersonal.app.rest.dto.PersonalDTO;
import br.com.gympersonal.app.rest.dto.PersonalModel;
import br.com.gympersonal.app.service.PersonalService;
import br.com.gympersonal.app.service.PlanoService;
import br.com.gympersonal.app.service.UsuarioService;
import br.com.gympersonal.app.model.entity.Personal;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/personais")
@RequiredArgsConstructor
@Data
@Getter
@Setter
public class PersonalController {

    @Autowired
    private PersonalRepository repository;
    
    @Autowired
    private PlanoRepository planoRepository;

    @Autowired
    private PlanoService planoService;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private ModelMapper modelMapper;
    
    @Autowired
    private PersonalService service;

    @Autowired private JavaMailSender mailSender;

    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PersonalModel salvar(@Valid @RequestBody PersonalDTO dto){
        if(usuarioRepository.existsByUsername(dto.getEmail()) == true){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O E-mail informado já existe no banco de dados");
        }

        if(!planoService.planoExists(dto.getPlanoId())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O Plano com ID:" + dto.getPlanoId() +  " não existe no banco de dados");
        }


        return service.save(dto);
    }

    @GetMapping("{id}")
    public PersonalModel buscarPorId(@PathVariable Integer id){

        return service.find(id);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable Integer id){
        service.delete(id);
    }
    

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void atualizar(@PathVariable("id") Integer id, @RequestBody PersonalDTO personal){
    	service.update(personal);
    }

    @PostMapping(path="/{id}/alterar-plano")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void alterarPlano(@PathVariable("id") Integer id, @RequestBody PersonalDTO dto){
        service.changePlano(id, dto);
    }

    @GetMapping
    public List<PersonalModel> listarTodos(){
        return service.listAll();
    }

    @PostMapping(path="/{id}/ativar")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void ativar(@PathVariable Integer id){
        service.activate(id);
    }

    @PostMapping(path="/{id}/desativar")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void desativar(@PathVariable Integer id){
        service.disable(id);
    }

    @GetMapping(path="/buscar-por-email/{email}")
    public List<PersonalModel> buscarPorEmail(@PathVariable String email){
       return service.findByEmail(email);
    }

    @GetMapping(path="/buscar-por-nome/{nome}")
    public List<PersonalModel> buscarPorNome(@PathVariable String nome){
        return service.findByNome(nome);
    }

    @GetMapping(path="/buscar-por-cpf/{cpf}")
    public List<PersonalModel> buscarPorCpf(@PathVariable String cpf){
        return service.findByCpf(cpf);
    }

    @PostMapping(path="/{id}/resetar-senha")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void resetarSenha(@PathVariable Integer id){
        service.resetPassword(id);
    }





    private PersonalDTO toPersonalDTO(Personal personal){
        return modelMapper.map(personal, PersonalDTO.class);
    }

    private PersonalModel toPersonalModel(Personal personal){
        return modelMapper.map(personal, PersonalModel.class);
    }

    private Personal toPersonal(PersonalDTO personal){
        return modelMapper.map(personal, Personal.class);
    }



}
