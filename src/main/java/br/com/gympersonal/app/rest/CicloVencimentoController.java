package br.com.gympersonal.app.rest;

import br.com.gympersonal.app.model.entity.CicloVencimento;
import br.com.gympersonal.app.model.repository.CicloVencimentoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/ciclos-vencimento")
@RequiredArgsConstructor
public class CicloVencimentoController {

    @Autowired
    private CicloVencimentoRepository repository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CicloVencimento salvar(@RequestBody @Valid CicloVencimento cicloVencimento){
        try {

            return repository.save(cicloVencimento);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    @GetMapping("{id}")
    public CicloVencimento buscarPorId(@PathVariable Integer id){
        return repository
                .findById(id)
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Ciclo de Vencimento Não Encontrado."));
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable Integer id){
        repository
                .findById(id)
                .map(cicloVencimento -> {
                    repository.delete(cicloVencimento);
                    return Void.TYPE;
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Ciclo de Vencimento Não Encontrado."));
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void atualizar(@PathVariable Integer id, @RequestBody @Valid CicloVencimento cicloAtualizado){
        repository
                .findById(id)
                .map(cicloVencimento -> {
                    cicloAtualizado.setId(cicloVencimento.getId());
                    return repository.save(cicloAtualizado);
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public List<CicloVencimento> listarTodos(){
        return repository.findAll();
    }

}


