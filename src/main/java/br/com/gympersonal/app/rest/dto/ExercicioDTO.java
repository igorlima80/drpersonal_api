package br.com.gympersonal.app.rest.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@Getter
@Setter
public class ExercicioDTO {
    private Integer id;

    @NotBlank(message="O nome é obrigatório")
    private String nome;

    @NotNull(message="O Tipo de Exercício é obrigatório.")
    private Integer tipoExercicioId;

    @NotNull(message="O Tipo de Métrica é obrigatório.")
    private Integer tipoMetricaId;

    private String caminhoVideo;

    private Integer personalId;

    @NotEmpty(message = "É obrigatório informar no mínimo um Grupo")
    private List<Integer> grupos;

    private List<MultipartFile> images;


}
