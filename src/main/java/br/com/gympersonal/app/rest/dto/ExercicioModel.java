package br.com.gympersonal.app.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExercicioModel {
    private Integer id;
    private String nome;
    private String status;
    private Integer tipoExercicioId;
    private Integer tipoMetricaId;
    private String caminhoVideo;
    private Integer personalId;
    private List<GrupoMuscularModel> grupos;
}
