package br.com.gympersonal.app.rest.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@Getter
@Setter
public class PlanoDTO {
    private Integer id;

    @NotBlank(message="O nome é obrigatório")
    private String nome;

    @Min(value = 0L, message = "O Valor deve ser positivo.")
    private Double valor;

    @NotNull(message="O Limite de Clientes é obrigatório.")
    @Min(value = 0, message = "O Limite de Clientes deve ser positivo.")
    private int limiteClientes;

    @NotNull(message="O Ciclo de Vencimento é obrigatório.")
    private Integer cicloVencimentoId;

}
