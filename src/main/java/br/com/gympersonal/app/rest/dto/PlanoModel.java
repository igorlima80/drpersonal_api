package br.com.gympersonal.app.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PlanoModel implements Serializable{
    private Integer id;
    private String nome;
    private String valor;
    private int limiteClientes;
    private int quantidadePersonais;
    private CicloVencimentoModel cicloVencimento;

}
