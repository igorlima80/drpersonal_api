package br.com.gympersonal.app.rest;

import br.com.gympersonal.app.model.entity.TipoMetrica;
import br.com.gympersonal.app.model.repository.TipoMetricaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tipos-metrica")
@RequiredArgsConstructor
public class TipoMetricaController {
    @Autowired
    private TipoMetricaRepository repository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TipoMetrica salvar(@RequestBody @Valid TipoMetrica tipoMetrica){
        try {
            return repository.save(tipoMetrica);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("{id}")
    public TipoMetrica buscarPorId(@PathVariable Integer id){
        return repository
                .findById(id)
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tipo de Métrica Não Encontrado."));
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable Integer id){
        repository
                .findById(id)
                .map(tipoMetrica -> {
                    repository.delete(tipoMetrica);
                    return Void.TYPE;
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tipo de Métrica Não Encontrado."));
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void atualizar(@PathVariable Integer id, @RequestBody @Valid TipoMetrica tipoMetricaAtualizado){
        repository
                .findById(id)
                .map(tipoMetrica -> {
                   tipoMetricaAtualizado.setId(tipoMetrica.getId());
                    return repository.save(tipoMetricaAtualizado);
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public List<TipoMetrica> listarTodos(){
        return repository.findAll();
    }

}
