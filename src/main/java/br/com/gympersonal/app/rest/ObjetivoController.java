package br.com.gympersonal.app.rest;

import br.com.gympersonal.app.model.entity.Objetivo;
import br.com.gympersonal.app.model.repository.ObjetivoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/objetivos")
@RequiredArgsConstructor
public class ObjetivoController {

    @Autowired
    private ObjetivoRepository repository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Objetivo salvar(@RequestBody @Valid Objetivo objetivo){
        try {
            return repository.save(objetivo);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("{id}")
    public Objetivo buscarPorId(@PathVariable Integer id){
        return repository
                .findById(id)
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Objetivo Não Encontrado."));
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable Integer id){
        repository
                .findById(id)
                .map(objetivo -> {
                    repository.delete(objetivo);
                    return Void.TYPE;
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Objetivo Não Encontrado."));
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void atualizar(@PathVariable Integer id, @RequestBody @Valid Objetivo objetivoAtualizado){
        repository
                .findById(id)
                .map(objetivo -> {
                    objetivoAtualizado.setId(objetivo.getId());
                    return repository.save(objetivoAtualizado);
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public List<Objetivo> listarTodos(){
        return repository.findAll();
    }

}
