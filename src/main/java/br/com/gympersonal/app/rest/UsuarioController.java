package br.com.gympersonal.app.rest;

import br.com.gympersonal.app.model.entity.Aluno;
import br.com.gympersonal.app.model.entity.Personal;
import br.com.gympersonal.app.model.repository.UsuarioRepository;
import br.com.gympersonal.app.rest.dto.UsuarioModel;
import br.com.gympersonal.app.service.UsuarioService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/usuarios")
@RequiredArgsConstructor
@Getter
@Setter
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @Autowired
    private UsuarioRepository repository;


    @RequestMapping(value = "/logado", method = RequestMethod.GET)
    @ResponseBody
    public UsuarioModel currentUser(Authentication authentication) {
        return repository
                .findByUsername(authentication.getName())
                .map(usuario -> {
                    UsuarioModel model = service.toUsuarioModel(usuario);
                    if(usuario.getPessoa() instanceof Personal){
                        model.setPerfil("PERSONAL");

                    }else if (usuario.getPessoa() instanceof Aluno){
                        model.setPerfil("ALUNO");
                    }else {
                        model.setPerfil("ADMIN");
                    }
                    return model;
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuário Não Encontrado."));
    }




}
