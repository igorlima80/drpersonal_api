package br.com.gympersonal.app.rest;


import br.com.gympersonal.app.model.repository.PersonalRepository;
import br.com.gympersonal.app.rest.dto.PlanoDTO;
import br.com.gympersonal.app.rest.dto.PlanoModel;
import br.com.gympersonal.app.service.PlanoService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/planos")
@RequiredArgsConstructor
public class PlanoController {

    @Autowired
    private PlanoService service;

    @Autowired
    private PersonalRepository personalRepository;

    @Autowired
    private ModelMapper modelMapper;




    @ApiOperation(value = "[x] O admin consegue cadastrar um plano com nome, valor, limite de clientes e ciclo de vencimento")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PlanoModel salvar(@Valid @RequestBody PlanoDTO dto){

        if(!service.cicloVencimentoExists(dto.getCicloVencimentoId())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O Ciclo de Vencimento com ID:" + dto.getCicloVencimentoId() +  " não existe no banco de dados");
        }

        return service.save(dto);
    }

    @GetMapping("{id}")
    public PlanoModel buscarPorId(@PathVariable Integer id){
        return service.findById(id);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable Integer id){
        service.delete(id);
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void atualizar(@PathVariable Integer id, @Valid @RequestBody PlanoDTO dto){
       service.update(id, dto);
    }

    @GetMapping
    public List<PlanoModel> listarTodos(){
       return service.listAll();
    }

    @ApiOperation(value = "[x] O admin consegue ativar ou desativar planos")
    @PostMapping(path="/alterar-status/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void alterarStatus(@PathVariable Integer id){
        service.statusChange(id);
    }



}
