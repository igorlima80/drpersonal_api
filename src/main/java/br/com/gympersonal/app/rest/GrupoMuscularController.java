package br.com.gympersonal.app.rest;


import br.com.gympersonal.app.model.entity.GrupoMuscular;
import br.com.gympersonal.app.model.repository.GrupoMuscularRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/grupos-musculares")
@RequiredArgsConstructor
public class GrupoMuscularController {

    @Autowired
    private GrupoMuscularRepository repository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public GrupoMuscular salvar(@RequestBody @Valid GrupoMuscular grupoMuscular){
        try {
            return repository.save(grupoMuscular);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("{id}")
    public GrupoMuscular buscarPorId(@PathVariable Integer id){
        return repository
                .findById(id)
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Grupo Muscular Não Encontrado."));
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable Integer id){
        repository
                .findById(id)
                .map(grupoMuscular -> {
                    repository.delete(grupoMuscular);
                    return Void.TYPE;
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Grupo Muscular Não Encontrado."));
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void atualizar(@PathVariable Integer id, @RequestBody @Valid GrupoMuscular grupoMuscularAtualizado){
        repository
                .findById(id)
                .map(grupoMuscular -> {
                    grupoMuscularAtualizado.setId(grupoMuscular.getId());
                    return repository.save(grupoMuscularAtualizado);
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public List<GrupoMuscular> listarTodos(){
        return repository.findAll();
    }

}
