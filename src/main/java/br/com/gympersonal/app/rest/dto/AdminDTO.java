package br.com.gympersonal.app.rest.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@Getter
@Setter
public class AdminDTO {
    private Long id;

    @NotBlank(message="O nome é obrigatório")
    private String nome;


    private String dataNascimento;
    private String cpf;
    private String telefone;
    private String sexo;

    @NotBlank(message="O E-mail é obrigatório.")
    @Email(message="O email está inválido.")
    private String email;

    private String isAdmin;


}
