package br.com.gympersonal.app.rest.dto;


import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class PersonalDTO {
        private Integer id;

        @NotBlank(message="O Nome é obrigatório")
        private String nome;
        private String dataNascimento;
        private String cpf;
        private String telefone;

        @NotBlank(message="O Sexo é obrigatório. Opções: MASCULINO ou FEMININO")
        private String sexo;

        @NotBlank(message="O E-mail é obrigatório.")
        @Email(message="O E-mail está inválido.")
        private String email;

        @NotNull(message="O Plano é obrigatório.")
        private Integer planoId;
    }
