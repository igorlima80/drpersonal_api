package br.com.gympersonal.app.rest.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Getter
@Setter
public class UsuarioModel {
    private Long id;
    private String statusUsuario;
    private String username;
    private String isAdmin;
    private String perfil;
    private PessoaModel pessoa;
}