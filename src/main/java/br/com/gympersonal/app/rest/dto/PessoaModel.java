package br.com.gympersonal.app.rest.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Getter
@Setter
public class PessoaModel {
    private Long id;
    private String nome;
    private String dataNascimento;
    private String cpf;
    private String telefone;
    private String sexo;
}

