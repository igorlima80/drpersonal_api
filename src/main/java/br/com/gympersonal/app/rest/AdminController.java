package br.com.gympersonal.app.rest;


import br.com.gympersonal.app.model.entity.Pessoa;

import br.com.gympersonal.app.model.repository.PessoaRepository;

import br.com.gympersonal.app.model.repository.UsuarioRepository;
import br.com.gympersonal.app.rest.dto.AdminDTO;
import br.com.gympersonal.app.service.AdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/admins")
@RequiredArgsConstructor
@Api(value = "Admin")
public class AdminController {

    @Autowired
    private PessoaRepository repository;

    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private AdminService service;

    @Autowired
    private ModelMapper modelMapper;

    @ApiOperation(value = "Cadastra um Administrador")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AdminDTO salvar(@Valid @RequestBody AdminDTO dto){
        if(usuarioRepository.existsByUsername(dto.getEmail()) == true){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O E-mail informado já existe no banco de dados");
        }
       return service.save(dto);
    }

    @GetMapping("{id}")
    public AdminDTO buscarPorId(@PathVariable Integer id){
        return service.find(id);
    }


    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable Integer id){
        service.delete(id);
    }

       
    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Pessoa> atualizar(@PathVariable("id") Integer id, @Valid @RequestBody Pessoa pessoa){
    	return service.update(id, pessoa);
    }

    @GetMapping
    public List<AdminDTO> listarTodos(){
     return service.listAll();
    }

    private AdminDTO toAdminDTO(Pessoa pessoa){
        return modelMapper.map(pessoa, AdminDTO.class);
    }

    private Pessoa toPessoa(AdminDTO admin){
        return modelMapper.map(admin, Pessoa.class);
    }






}
