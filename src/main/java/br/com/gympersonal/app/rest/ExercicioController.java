package br.com.gympersonal.app.rest;



import br.com.gympersonal.app.service.ExercicioService;
import br.com.gympersonal.app.rest.dto.ExercicioDTO;
import br.com.gympersonal.app.rest.dto.ExercicioFiltroDTO;
import br.com.gympersonal.app.rest.dto.ExercicioModel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/exercicios")
@RequiredArgsConstructor
public class ExercicioController {


    @Autowired
    private ExercicioService service;

    @PostMapping(consumes = { "multipart/form-data" })
    @ResponseStatus(HttpStatus.CREATED)
    public ExercicioModel salvar(@Valid @RequestBody ExercicioDTO dto, @RequestParam("images") MultipartFile[] images){

        dto.getGrupos().forEach(item -> {
            System.out.println("item " + item);
            if(!service.grupoExists(item)){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O grupo com ID:" + item +  " não existe no banco de dados");
            }
        });

        if(!service.tipoExercicioExists(dto.getTipoExercicioId())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O Tipo de Exercício com ID:" + dto.getTipoExercicioId() +  " não existe no banco de dados");
        }

        if(!service.tipoMetricaExists(dto.getTipoMetricaId())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O Tipo de Métrica com ID:" + dto.getTipoMetricaId() +  " não existe no banco de dados");
        }

        return service.save(dto, images);
    }

    @GetMapping("{id}")
    public ExercicioModel buscarPorId(@PathVariable Integer id){
        return service.find(id);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable Integer id){
        service.delete(id);
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void atualizar(@PathVariable("id") Integer id, @RequestBody ExercicioDTO exercicio){
         service.update(id, exercicio);
    }

    @PostMapping(path="/{id}/ativar")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void ativar(@PathVariable Integer id){
        service.activate(id);
    }

    @PostMapping(path="/{id}/desativar")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void desativar(@PathVariable Integer id){
        service.inactivate(id);
    }

    @GetMapping(path="/listar-globais")
    public List<ExercicioModel> listarGlobais(){
       return service.listGlobals();
    }

    @GetMapping(path="/listar-por-personal/{id}")
    public List<ExercicioModel> listarPorPersonalId(@PathVariable int id){
        return service.listByPersonalId(id);
    }

    @GetMapping(path="/filtrar-globais-por-grupo-muscular/{id}")
    public List<ExercicioModel> listarPorGrupoId(@PathVariable int id){
        return service.listGlobalsByGrupo(id);
    }

    @PostMapping(path="/filtrar-por-personal-e-por-grupo-muscular")
    public List<ExercicioModel> listarPorPersonalGrupoId(@Valid @RequestBody ExercicioFiltroDTO dto){
        return service.listByPersonalAndGrupo(dto.getIdPersonal(), dto.getIdGrupoMuscular());
    }



}
