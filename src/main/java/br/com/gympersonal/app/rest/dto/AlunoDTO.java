package br.com.gympersonal.app.rest.dto;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
    public class AlunoDTO {
        private Integer id;

        @NotBlank(message="O nome é obrigatório")
        private String nome;

        private String dataNascimento;
        private String cpf;
        private String telefone;
        private String sexo;

        @NotBlank(message="O E-mail é obrigatório.")
        @Email(message="O email está inválido.")
        private String email;

        @NotNull(message="O Personal é obrigatório.")
        private Integer personalId;
    }
