package br.com.gympersonal.app.rest.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PersonalModel implements Serializable{
    private Integer id;
    private String nome;
    private String dataNascimento;
    private String cpf;
    private String telefone;
    private String sexo;
    private String email;
    private String isAdmin;
    private int quantidadeAlunos;
    private PlanoModel plano;
}
