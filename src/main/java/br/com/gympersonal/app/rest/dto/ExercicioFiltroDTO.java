package br.com.gympersonal.app.rest.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@Getter
@Setter
public class ExercicioFiltroDTO {
    @NotNull(message="O Personal é obrigatório.")
    private Integer idPersonal;

    @NotNull(message="O Grupo Muscular é obrigatório.")
    private Integer idGrupoMuscular;
}
