package br.com.gympersonal.app.rest.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Getter
@Setter
public class CicloVencimentoModel {
    private Integer id;
    private String descricao;
    private String quantidadeDias;
}
