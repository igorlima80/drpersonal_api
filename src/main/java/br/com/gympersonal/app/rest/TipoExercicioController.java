package br.com.gympersonal.app.rest;


import br.com.gympersonal.app.model.entity.TipoExercicio;
import br.com.gympersonal.app.model.repository.TipoExercicioRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tipos-exercicio")
@RequiredArgsConstructor
public class TipoExercicioController {
    @Autowired
    private TipoExercicioRepository repository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TipoExercicio salvar(@RequestBody @Valid TipoExercicio tipoExercicio){
        try {
            return repository.save(tipoExercicio);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("{id}")
    public TipoExercicio buscarPorId(@PathVariable Integer id){
        return repository
                .findById(id)
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tipo Exercício Não Encontrado."));
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable Integer id){
        repository
                .findById(id)
                .map(tipoExercicio -> {
                    repository.delete(tipoExercicio);
                    return Void.TYPE;
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tipo Exercício Não Encontrado."));
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void atualizar(@PathVariable Integer id, @RequestBody @Valid TipoExercicio tipoExercicioAtualizado){
        repository
                .findById(id)
                .map(tipoExercicio -> {
                    tipoExercicioAtualizado.setId(tipoExercicio.getId());
                    return repository.save(tipoExercicioAtualizado);
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public List<TipoExercicio> listarTodos(){
        return repository.findAll();
    }
}
