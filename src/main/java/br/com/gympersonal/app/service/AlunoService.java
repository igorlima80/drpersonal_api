package br.com.gympersonal.app.service;

import br.com.gympersonal.app.core.UsuarioUtil;
import br.com.gympersonal.app.model.entity.Aluno;
import br.com.gympersonal.app.model.entity.Sexo;
import br.com.gympersonal.app.model.entity.Usuario;
import br.com.gympersonal.app.model.repository.AlunoRepository;
import br.com.gympersonal.app.rest.dto.AlunoDTO;
import br.com.gympersonal.app.rest.dto.AlunoModel;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Service
public class AlunoService {

    @Autowired
    private AlunoRepository repository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UsuarioService usuarioService;

    @Transactional
    public AlunoModel save(AlunoDTO dto) {

            LocalDate data = LocalDate.parse(dto.getDataNascimento(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));

            Aluno aluno = toAluno(dto);
            aluno.setDataNascimento(data);
            aluno.setSexo(Sexo.valueOf(dto.getSexo().toUpperCase()));

            Usuario usuario = UsuarioUtil.criarUsuario(dto.getEmail());
            usuario.setPessoa(aluno);
            aluno.setUsuario(usuario);

            repository.save(aluno);
            usuarioService.sendMailCreateAccount(usuario);

            return toAlunoModel(aluno);

    }

    @Transactional
    public AlunoModel find(Integer id) {
        Aluno aluno = repository
                .findById(id)
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Aluno Não Encontrado."));

        AlunoModel am = toAlunoModel(aluno);

        return am;
    }

    @Transactional
    public void delete(Integer id) {
        repository
                .findById(id)
                .map(aluno -> {
                    repository.delete(aluno);
                    return Void.TYPE;
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Aluno Não Encontrado."));
    }

    public void update(Integer id, AlunoDTO alunoAtualizado){
        repository
                .findById(id)
                .map(aluno -> {
                    alunoAtualizado.setId(aluno.getId());

                    alunoAtualizado.setSexo(alunoAtualizado.getSexo().toUpperCase());
                    return repository.save(toAluno(alunoAtualizado));
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }



    private AlunoModel toAlunoModel(Optional<Aluno> obj){
        return modelMapper.map(obj, AlunoModel.class);
    }

    private AlunoDTO toAlunoDTO(Aluno aluno){
        return modelMapper.map(aluno, AlunoDTO.class);
    }

    private AlunoModel toAlunoModel(Aluno aluno){
        return modelMapper.map(aluno, AlunoModel.class);
    }

    private Aluno toAluno(AlunoDTO aluno){
        return modelMapper.map(aluno, Aluno.class);
    }

}
