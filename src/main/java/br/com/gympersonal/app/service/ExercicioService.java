package br.com.gympersonal.app.service;


import br.com.gympersonal.app.model.entity.Exercicio;
import br.com.gympersonal.app.model.entity.GrupoMuscular;
import br.com.gympersonal.app.model.entity.StatusExercicio;
import br.com.gympersonal.app.model.repository.ExercicioRepository;
import br.com.gympersonal.app.model.repository.GrupoMuscularRepository;
import br.com.gympersonal.app.model.repository.TipoExercicioRepository;
import br.com.gympersonal.app.model.repository.TipoMetricaRepository;

import br.com.gympersonal.app.model.entity.Foto;
import br.com.gympersonal.app.rest.dto.ExercicioDTO;
import br.com.gympersonal.app.rest.dto.ExercicioModel;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExercicioService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ExercicioRepository repository;

    @Autowired
    private TipoExercicioRepository tipoExercicioRepository;

    @Autowired
    private TipoMetricaRepository tipoMetricaRepository;

    @Autowired
    private GrupoMuscularRepository grupoRepository;

    @Autowired
    private FilesStorageService storageService;

    @Transactional
    public ExercicioModel save(ExercicioDTO dto, MultipartFile[] images) {
        try {
            Exercicio exercicio = toExercicio(dto);
            exercicio.setStatus(StatusExercicio.INATIVO);

            dto.getGrupos().forEach(item -> {
                exercicio.getGrupos().add(grupoRepository.getById(item));
            });

            Arrays.asList(images).stream().forEach(file -> {
                storageService.save(file);

                Foto foto = new Foto(file.getOriginalFilename(), "");
                exercicio.getFotos().add(foto);
            });

            return toExercicioModel(repository.save(exercicio));

        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @Transactional
    public ExercicioModel find(Integer id) {
        Exercicio exercicio = repository
                .findById(id)
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Exercício Não Encontrado."));

        ExercicioModel exercicioModel = toExercicioModel(exercicio);

        return exercicioModel;
    }

    @Transactional
    public void delete(Integer id) {
        repository
                .findById(id)
                .map(exercicio -> {
                    repository.delete(exercicio);
                    return Void.TYPE;
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Exercício Não Encontrado."));
    }


    public void update(@PathVariable Integer id, @RequestBody ExercicioDTO dto){
        repository
                .findById(id)
                .map(exercicio -> {
                    exercicio.setTipoExercicio(tipoExercicioRepository.getById(dto.getTipoExercicioId()));
                    exercicio.setTipoMetrica(tipoMetricaRepository.getById(dto.getTipoMetricaId()));
                    exercicio.setNome(dto.getNome());
                    exercicio.setCaminhoVideo(dto.getCaminhoVideo());

                    List<GrupoMuscular> lista= new ArrayList<GrupoMuscular>();

                    dto.getGrupos().forEach(item ->{
                        lista.add(grupoRepository.getById(item));
                    });

                    exercicio.setGrupos(lista);

                    return repository.save(exercicio);
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }


    public void activate(@PathVariable Integer id){
        repository
                .findById(id)
                .map(exercicio -> {
                    exercicio.setStatus(StatusExercicio.ATIVO);

                    return repository.save(exercicio);
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public void inactivate(@PathVariable Integer id){
        repository
                .findById(id)
                .map(exercicio -> {
                    exercicio.setStatus(StatusExercicio.INATIVO);

                    return repository.save(exercicio);
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public List<ExercicioModel> listGlobals(){
        return repository
                .findByPersonalIsNull()
                .stream()
                .map(this::toExercicioModel)
                .collect(Collectors.toList());
    }

    public List<ExercicioModel> listByPersonalId(int id){
        return repository
                .findByPersonalId(id)
                .stream()
                .map(this::toExercicioModel)
                .collect(Collectors.toList());
    }

    public List<ExercicioModel> listByGrupoId(int id){
        return repository
                .findByGrupos_id(id)
                .stream()
                .map(this::toExercicioModel)
                .collect(Collectors.toList());
    }

    public List<ExercicioModel> listGlobalsByGrupo(int id){
        return repository
                .findByPersonalIsNullAndGrupos_id(id)
                .stream()
                .map(this::toExercicioModel)
                .collect(Collectors.toList());
    }

    public List<ExercicioModel> listByPersonalAndGrupo(int idPersonal, int idGrupo){
        return repository
                .findByPersonalIdAndGrupos_id(idPersonal, idGrupo)
                .stream()
                .map(this::toExercicioModel)
                .collect(Collectors.toList());
    }

    public boolean grupoExists(Integer id){
        return grupoRepository.existsById(id);
    }

    public boolean tipoExercicioExists(Integer id){
        return tipoExercicioRepository.existsById(id);
    }

    public boolean tipoMetricaExists(Integer id){
        return tipoMetricaRepository.existsById(id);
    }

    private void addFotos(Integer exercicioId, MultipartFile[] images){
        List<String> fileNames = new ArrayList<>();

        Arrays.asList(images).stream().forEach(file -> {
            storageService.save(file);
            fileNames.add(file.getOriginalFilename());

            Foto foto = new Foto(file.getOriginalFilename(), "");
            foto.setExercicio(repository.getById(exercicioId));
        });
    }

    private ExercicioDTO toExercicioDTO(Exercicio exercicio){
        return modelMapper.map(exercicio, ExercicioDTO.class);
    }

    private ExercicioModel toExercicioModel(Exercicio exercicio){
        return modelMapper.map(exercicio, ExercicioModel.class);
    }

    private Exercicio toExercicio(ExercicioDTO dto){
        return modelMapper.map(dto, Exercicio.class);
    }
}
