package br.com.gympersonal.app.service;


import br.com.gympersonal.app.model.entity.Aluno;
import br.com.gympersonal.app.model.entity.Personal;
import br.com.gympersonal.app.model.entity.Usuario;
import br.com.gympersonal.app.model.repository.UsuarioRepository;

import br.com.gympersonal.app.rest.dto.UsuarioModel;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class UsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository repository;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private ModelMapper modelMapper;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = repository
                                .findByUsername(username)
                                .orElseThrow(() -> new UsernameNotFoundException("Login Não Encontrado."));

        UsuarioModel model = toUsuarioModel(usuario);
        if(usuario.getPessoa() instanceof Personal){
            model.setPerfil("PERSONAL");

        }else if (usuario.getPessoa() instanceof Aluno){
            model.setPerfil("ALUNO");
        }else {
            model.setPerfil("ADMIN");
        }


        return User
                .builder()
                .username(usuario.getUsername())
                .password(usuario.getSenha())
                .roles(model.getPerfil())
                .build();
    }



    public String sendMailCreateAccount(Usuario usuario) {
        try {
            MimeMessage mail = mailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper( mail );
            helper.setTo(usuario.getUsername());
            helper.setSubject( "Dr. Personal - Criação de Conta" );
            String texto = new String("<p>Olá! Sua conta foi criada no App Dr. Personal</p>")
                    .concat("<p>Seu usuário é: " + usuario.getUsername() + "</p>")
                    .concat("A senha inicial de acesso é: " + usuario.getSenha() + "</p>");

            helper.setText(texto, true);

            mailSender.send(mail);

            return "OK";
        } catch (Exception e) {
            e.printStackTrace();
            return "Erro ao enviar e-mail";
        }
    }

    public String sendMailResetPassword(Usuario usuario) {
        try {
            MimeMessage mail = mailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper( mail );
            helper.setTo( "f.igor.lima@gmail.com" );
            helper.setSubject( "Dr. Personal - Alteração de Conta" );
            String texto = new String("<p>Olá! Sua senha foi resetada no App Dr. Personal</p>")
                    .concat("<p>Seu usuário é: " + usuario.getUsername() + "</p>")
                    .concat("A nova de acesso é: " + usuario.getSenha() + "</p>");

            helper.setText(texto, true);

            mailSender.send(mail);

            return "OK";
        } catch (Exception e) {
            e.printStackTrace();
            return "Erro ao enviar e-mail";
        }
    }

    public UsuarioModel toUsuarioModel(Usuario usuario){
        return modelMapper.map(usuario, UsuarioModel.class);
    }
    
   
}
