package br.com.gympersonal.app.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


import br.com.gympersonal.app.model.entity.Personal;
import br.com.gympersonal.app.model.entity.Sexo;
import br.com.gympersonal.app.model.entity.StatusUsuario;
import br.com.gympersonal.app.model.entity.Usuario;
import br.com.gympersonal.app.model.repository.AlunoRepository;
import br.com.gympersonal.app.model.repository.UsuarioRepository;
import br.com.gympersonal.app.core.UsuarioUtil;
import br.com.gympersonal.app.model.repository.PlanoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import br.com.gympersonal.app.model.repository.PersonalRepository;
import br.com.gympersonal.app.rest.dto.PersonalDTO;
import br.com.gympersonal.app.rest.dto.PersonalModel;

@Service
public class PersonalService {

	@Autowired
	private PersonalRepository repository;
	
	@Autowired
	private PlanoRepository planoRepository;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private AlunoRepository alunoRepository;

	@Autowired
	private ModelMapper modelMapper;



	@Transactional
	public PersonalModel save(PersonalDTO dto) {

            LocalDate data = LocalDate.parse(dto.getDataNascimento(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));

            Personal personal = toPersonal(dto);
            personal.setDataNascimento(data);
            personal.setSexo(Sexo.valueOf(dto.getSexo().toUpperCase()));
            
            Usuario usuario = UsuarioUtil.criarUsuario(dto.getEmail());
            usuario.setPessoa(personal);
            personal.setUsuario(usuario);

			repository.save(personal);
			usuarioService.sendMailCreateAccount(usuario);

            return toPersonalModel(personal);


	}

	@Transactional
	public PersonalModel find(Integer id) {
		Personal personal = repository
				.findById(id)
				.orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Personal Não Encontrado."));

		PersonalModel pm = toPersonalModel(personal);
		pm.setQuantidadeAlunos(alunoRepository.countByPersonalId(id));

		return pm;
	}
	
	@Transactional
	public void delete(Integer id) {
		 repository
         .findById(id)
         .map(personal -> {
             repository.delete(personal);
             return Void.TYPE;
         })
         .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Personal Não Encontrado."));
	}
	
	@Transactional
	public void update(PersonalDTO dto) {
		repository
        .findById(dto.getId())
        .map(personal -> {
        	
            personal.setNome(dto.getNome());
            personal.setDataNascimento(LocalDate.parse(dto.getDataNascimento(), DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            personal.setCpf(dto.getCpf());
            personal.setTelefone(dto.getTelefone());
            personal.setSexo(Sexo.valueOf(dto.getSexo().toUpperCase()));
            personal.setPlano(planoRepository.getById(dto.getId()));
            
            return repository.save(personal);
        })
        .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	@Transactional
	public void changePlano(Integer id, PersonalDTO dto){
		repository
				.findById(id)
				.map(personal -> {
					personal.setPlano(planoRepository.getById(dto.getPlanoId()));
					return repository.save(personal);
				})
				.orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	public List<PersonalModel> listAll(){
		return repository
				.findAll()
				.stream()
				.map(this::toPersonalModel)
				.collect(Collectors.toList());
	}

	@Transactional
	public void activate(Integer id){
		repository
				.findById(id)
				.map(personal -> {
					personal.getUsuario().setStatus(StatusUsuario.ATIVO);

					return usuarioRepository.save(personal.getUsuario());
				})
				.orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	@Transactional
	public void disable(Integer id){
		repository
				.findById(id)
				.map(personal -> {
					personal.getUsuario().setStatus(StatusUsuario.INATIVO);

					return usuarioRepository.save(personal.getUsuario());
				})
				.orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	public List<PersonalModel> findByEmail(String email){
		return repository
				.findByUsuarioUsernameLike("%" + email + "%")
				.stream()
				.map(this::toPersonalModel)
				.collect(Collectors.toList());
	}

	public List<PersonalModel> findByNome(String nome){
		return repository
				.findByNomeLike("%" + nome + "%")
				.stream()
				.map(this::toPersonalModel)
				.collect(Collectors.toList());
	}

	public List<PersonalModel> findByCpf(String cpf){
		return repository
				.findByCpfLike("%" + cpf + "%")
				.stream()
				.map(this::toPersonalModel)
				.collect(Collectors.toList());
	}

	public void resetPassword(Integer id){
		repository
				.findById(id)
				.map(personal -> {
					personal.getUsuario().setSenha(UsuarioUtil.generateCommonLangPassword());
					usuarioRepository.save(personal.getUsuario());
					return
							usuarioService.sendMailResetPassword(personal.getUsuario());
				})
				.orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	public boolean existsById(Integer id){
		return repository.existsById(id);
	}
	
	
	private PersonalModel toPersonalModel(Optional<Personal> obj){
	        return modelMapper.map(obj, PersonalModel.class);
	}

	private PersonalDTO toPersonalDTO(Personal personal){
	        return modelMapper.map(personal, PersonalDTO.class);
	}

	private PersonalModel toPersonalModel(Personal personal){
	        return modelMapper.map(personal, PersonalModel.class);
	    }

	private Personal toPersonal(PersonalDTO personal){
	        return modelMapper.map(personal, Personal.class);
	    }

	
}
