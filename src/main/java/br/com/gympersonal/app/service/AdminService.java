package br.com.gympersonal.app.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import br.com.gympersonal.app.core.UsuarioUtil;
import br.com.gympersonal.app.model.entity.Pessoa;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.gympersonal.app.model.entity.Sexo;
import br.com.gympersonal.app.model.entity.Usuario;
import br.com.gympersonal.app.model.repository.PessoaRepository;
import br.com.gympersonal.app.rest.dto.AdminDTO;

@Service
public class AdminService {
	
	  @Autowired
	    private PessoaRepository repository;

	    @Autowired
	    private ModelMapper modelMapper;
	    
	    public AdminDTO save(AdminDTO dto) {
	    	 try {
	             LocalDate data = LocalDate.parse(dto.getDataNascimento(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));

	             Pessoa admin = toPessoa(dto);
	             admin.setDataNascimento(data);
	             
	             Usuario usuario = UsuarioUtil.criarUsuarioAdmin(dto.getEmail());
	             usuario.setPessoa(admin);
	             admin.setUsuario(usuario);
	            

	             return toAdminDTO(repository.save(admin) );

	         }catch (Exception e){
	             throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
	         }
	    }
	    
	    public AdminDTO find(Integer id) {
	    	Pessoa pessoa = repository
	                .findById(id)
	                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Administrador Não Encontrado."));
	        
	    	return toAdminDTO(pessoa);
	    }
	    
	    public void delete(Integer id) {
	    	repository
            .findById(id)
            .map(pessoa -> {
                repository.delete(pessoa);
                return Void.TYPE;
            })
            .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Administrador Não Encontrado."));
	    }
	    
	    public ResponseEntity<Pessoa> update(Integer id, Pessoa pessoa) {
	    	return repository.findById(id)
	    	           .map(record -> {
	    	        	   record.setNome(pessoa.getNome());
	    	               record.setDataNascimento(pessoa.getDataNascimento());
	    	               record.setCpf(pessoa.getCpf());
	    	               record.setTelefone(pessoa.getTelefone());
	    	               record.setSexo(Sexo.valueOf(pessoa.getSexo().toString().toUpperCase()));
	    	                   	               	        
	    	               Pessoa updated = repository.save(record);
	    	               return ResponseEntity.ok().body(updated);
	    	           }).orElse(ResponseEntity.notFound().build());
	    }
	    
	    public List<AdminDTO> listAll(){
	    	return repository
	                .buscarAdministradores()
	                .stream()
	                .map(this::toAdminDTO)
	                .collect(Collectors.toList());
	    }
	    
	    
	    private AdminDTO toAdminDTO(Pessoa pessoa){
	        return modelMapper.map(pessoa, AdminDTO.class);
	    }

	    private Pessoa toPessoa(AdminDTO admin){
	        return modelMapper.map(admin, Pessoa.class);
	    }





}
