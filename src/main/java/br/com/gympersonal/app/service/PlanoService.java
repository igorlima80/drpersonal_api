package br.com.gympersonal.app.service;

import br.com.gympersonal.app.model.entity.Plano;
import br.com.gympersonal.app.model.entity.StatusPlano;
import br.com.gympersonal.app.model.repository.CicloVencimentoRepository;
import br.com.gympersonal.app.model.repository.PersonalRepository;
import br.com.gympersonal.app.model.repository.PlanoRepository;
import br.com.gympersonal.app.rest.dto.PlanoDTO;
import br.com.gympersonal.app.rest.dto.PlanoModel;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlanoService {

    @Autowired
    private PlanoRepository repository;

    @Autowired
    private PersonalRepository personalRepository;

    @Autowired
    private CicloVencimentoRepository cicloRepository;

    @Autowired
    private ModelMapper modelMapper;

    public PlanoModel save(PlanoDTO dto){
        Plano plano = toPlano(dto);
        plano.setStatus(StatusPlano.INATIVO);
        return toPlanoModel(repository.save(plano));
    }

    public void statusChange(Integer id){
        repository
                .findById(id)
                .map(plano -> {
                    if(plano.getStatus().equals(StatusPlano.ATIVO)){
                        plano.setStatus(StatusPlano.INATIVO);
                    }else{
                        plano.setStatus(StatusPlano.ATIVO);
                    }
                    return repository.save(plano);
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Plano não encontrado."));
    }

    public PlanoModel findById(Integer id){
        Plano plano = repository
                .findById(id)
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Plano Não Encontrado."));

        PlanoModel pm = toPlanoModel(plano);
        pm.setQuantidadePersonais(personalRepository.countByPlanoId(id));

        return pm;
    }

    public void delete(Integer id){
        repository
                .findById(id)
                .map(plano -> {
                    repository.delete(plano);
                    return Void.TYPE;
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Plano Não Encontrado."));
    }

    public void update(Integer id, PlanoDTO dto){
        Plano planoAtualizado = toPlano(dto);
        repository
                .findById(id)
                .map(plano -> {
                    planoAtualizado.setId(plano.getId());
                    planoAtualizado.setStatus(plano.getStatus());
                    return repository.save(planoAtualizado);
                })
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public List<PlanoModel> listAll(){
        return repository
                .findAll()
                .stream()
                .map(this::toPlanoModel)
                .collect(Collectors.toList());
    }

    public boolean cicloVencimentoExists(Integer id){
        return cicloRepository.existsById(id);
    }

    public boolean planoExists(Integer id){
        return repository.existsById(id);
    }



    private PlanoDTO toPlanoDTO(Plano plano){
        return modelMapper.map(plano, PlanoDTO.class);
    }

    private PlanoModel toPlanoModel(Plano plano){
        return modelMapper.map(plano, PlanoModel.class);
    }

    private Plano toPlano(PlanoDTO dto){
        return modelMapper.map(dto, Plano.class);
    }
}
